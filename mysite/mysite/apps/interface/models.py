#coding=utf-8

from django.db import models
from django.core.urlresolvers import reverse


# Create your models here.
class InterfaceEdition(models.Model):

    edition_name = models.CharField(max_length=255, verbose_name='版本名称')
    edition_code = models.IntegerField(verbose_name='版本号', default=0)
    edition_help = models.CharField(max_length=255, verbose_name='帮助', blank=True, null=True)

    def __str__(self):

        return '%s--%s' % (self.edition_name, self.edition_code)

    class Meta:

        unique_together = (("edition_name", "edition_code"),)  # 共同唯一验证



class InterfaceItems(models.Model):

    name = models.CharField(max_length=255, verbose_name='接口名称')
    url = models.CharField(max_length=255, verbose_name='接口地址')
    interface = models.ForeignKey(InterfaceEdition)

    def __str__(self):
        return self.name

    def get_absolute_url(self):

        return reverse('interface:interface_detail', kwargs={'item_id': self.id})


class InterfaceParame(models.Model):

    parame_key = models.CharField(max_length=10, verbose_name='parame key')
    parame_value = models.CharField(max_length=10, verbose_name='parame value')

    item = models.ForeignKey(InterfaceItems)

    def __str__(self):

        return '%s=%s' % (self.parame_key, self.parame_value)