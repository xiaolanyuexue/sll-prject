from django.conf.urls import url

from .import views

urlpatterns = [
    url(r'^item/(?P<interface_id>\d+)/$', views.InterfaceListPage.as_view(), name='interface_list'),
    url(r'^item/(?P<item_id>\d+)/detail/$', views.InterfaceDetail.as_view(), name='interface_detail'),
    url(r'^item/(?P<item_id>\d+)/update/$', views.InterfaceUpdate.as_view(), name='interface_update')
]