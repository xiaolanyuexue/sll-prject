# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='InterfaceEdition',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('edition_name', models.CharField(max_length=255, verbose_name=b'\xe7\x89\x88\xe6\x9c\xac\xe5\x90\x8d\xe7\xa7\xb0')),
                ('edition_code', models.IntegerField(default=0, unique=True, verbose_name=b'\xe7\x89\x88\xe6\x9c\xac\xe5\x8f\xb7')),
            ],
        ),
        migrations.AlterUniqueTogether(
            name='interfaceedition',
            unique_together=set([('edition_name', 'edition_code')]),
        ),
    ]
