# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('interface', '0004_interfaceitems'),
    ]

    operations = [
        migrations.CreateModel(
            name='InterfaceParame',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('parame_key', models.CharField(max_length=10, verbose_name=b'parame key')),
                ('parame_value', models.CharField(max_length=10, verbose_name=b'parame value')),
                ('item', models.ForeignKey(to='interface.InterfaceItems')),
            ],
        ),
    ]
