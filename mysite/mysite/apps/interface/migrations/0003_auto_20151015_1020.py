# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('interface', '0002_interfaceedition_edition_help'),
    ]

    operations = [
        migrations.AlterField(
            model_name='interfaceedition',
            name='edition_code',
            field=models.IntegerField(default=0, verbose_name=b'\xe7\x89\x88\xe6\x9c\xac\xe5\x8f\xb7'),
        ),
    ]
