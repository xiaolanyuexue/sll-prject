# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('interface', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='interfaceedition',
            name='edition_help',
            field=models.CharField(max_length=255, null=True, verbose_name=b'\xe5\xb8\xae\xe5\x8a\xa9', blank=True),
        ),
    ]
