from django.template.response import TemplateResponse
from django.views.generic.list import ListView
from django.views.generic.detail import DetailView
from django.views.generic.edit import UpdateView
from .models import InterfaceItems
from django.shortcuts import redirect
from .forms import InterfaceItemsForm, InterfaceItemInlineFormSet

# Create your views here.


class FormsetMixin(object):
    object = None

    def get(self, request, *args, **kwargs):
        if getattr(self, 'is_update_view', False):
            self.object = self.get_object()
        form_class = self.get_form_class()
        form = self.get_form(form_class)
        formset_class = self.get_formset_class()
        formset = self.get_formset(formset_class)
        return self.render_to_response(self.get_context_data(form=form, formset=formset))

    def post(self, request, *args, **kwargs):
        if getattr(self, 'is_update_view', False):
            self.object = self.get_object()
        form_class = self.get_form_class()
        form = self.get_form(form_class)
        formset_class = self.get_formset_class()
        formset = self.get_formset(formset_class)
        if form.is_valid() and formset.is_valid():
            return self.form_valid(form, formset)
        else:
            return self.form_invalid(form, formset)

    def get_formset_class(self):
        return self.formset_class

    def get_formset(self, formset_class):
        return formset_class(**self.get_formset_kwargs())

    def get_formset_kwargs(self):
        kwargs = {
            'instance': self.object
        }
        if self.request.method in ('POST', 'PUT'):
            kwargs.update({
                'data': self.request.POST,
                'files': self.request.FILES,
            })
        return kwargs

    def form_valid(self, form, formset):
        self.object = form.save()
        formset.instance = self.object
        formset.save()
        return redirect(self.object.get_absolute_url())

    def form_invalid(self, form, formset):
        return self.render_to_response(self.get_context_data(form=form, formset=formset))



class InterfaceListPage(ListView):
    template_name = 'interface/list.html'
    model = InterfaceItems

    def get_queryset(self):
        """
        Get the list of items for this view. This must be an iterable, and may
        be a queryset (in which qs-specific behavior will be enabled).
        """
        queryset = super(InterfaceListPage, self).get_queryset()
        return queryset.filter(interface__id=self.kwargs.get('interface_id'))



class InterfaceDetail(DetailView):

    template_name = 'interface/detail.html'
    model = InterfaceItems
    pk_url_kwarg = 'item_id'



class InterfaceUpdate(FormsetMixin, UpdateView):

    template_name = 'interface/update.html'
    model = InterfaceItems
    pk_url_kwarg = 'item_id'
    is_update_view = True
    form_class = InterfaceItemsForm
    formset_class = InterfaceItemInlineFormSet


