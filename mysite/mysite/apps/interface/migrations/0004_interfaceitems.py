# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('interface', '0003_auto_20151015_1020'),
    ]

    operations = [
        migrations.CreateModel(
            name='InterfaceItems',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=255, verbose_name=b'\xe6\x8e\xa5\xe5\x8f\xa3\xe5\x90\x8d\xe7\xa7\xb0')),
                ('url', models.CharField(max_length=255, verbose_name=b'\xe6\x8e\xa5\xe5\x8f\xa3\xe5\x9c\xb0\xe5\x9d\x80')),
                ('interface', models.ForeignKey(to='interface.InterfaceEdition')),
            ],
        ),
    ]
