from django.template.response import TemplateResponse
from django.views.generic.base import TemplateView
from django.views.generic.list import ListView
from django.contrib.auth import authenticate, login, logout
from django.shortcuts import redirect
from .forms import UserForm
from mysite.apps.interface.models import InterfaceEdition
# Create your views here.

class HomePage(TemplateView):

    template_name = 'home.html'

    def post(self, request, *args, **kwargs):
        context = self.get_context_data(*args, **kwargs)
        form = UserForm(request, request.POST)
        if form.is_valid():
            user = authenticate(username=request.POST.get('username'), password=request.POST.get('password'))
            if user is not None:
                login(request, user)
                return redirect('/login-success/')
        context['form'] = form
        return self.render_to_response(context)


class InterfaceList(ListView):
    template_name = 'login_success.html'
    model = InterfaceEdition

    def get_context_object_name(self, object_list):
        return super(InterfaceList, self).get_context_object_name(object_list)


def logout_view(request):
    logout(request)
    return redirect('/home/')