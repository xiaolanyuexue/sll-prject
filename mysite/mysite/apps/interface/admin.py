from django.contrib import admin
from .models import InterfaceEdition, InterfaceItems
# Register your models here.

admin.site.register(InterfaceEdition)
admin.site.register(InterfaceItems)
