__author__ = 'sll'
from django.contrib.auth.models import User
from django.forms import forms
from django.contrib.auth.forms import AuthenticationForm
from django.contrib.auth import authenticate


class UserForm(AuthenticationForm):

    def clean_username(self):
        username = self.cleaned_data.get('username')
        if not User.objects.filter(username=username).exists():
            raise forms.ValidationError(['User is not exist!'])
        return username

    def clean(self):

        username = self.cleaned_data.get('username')
        password = self.cleaned_data.get('password')
        if not authenticate(username=username, password=password):
            self._errors['password'] = self.error_class(['Password is error!'])
        return self.cleaned_data