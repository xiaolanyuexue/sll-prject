from django.conf.urls import url

from .import views

urlpatterns = [
    url(r'^home/$', views.HomePage.as_view(), name='index'),
    url(r'^login-success/$', views.InterfaceList.as_view(), name='success'),
    url(r'^logout/$', views.logout_view, name='success'),
]