__author__ = 'sll'
from django.forms.models import inlineformset_factory
from .models import InterfaceItems, InterfaceEdition, InterfaceParame
from django.forms import ModelForm
from django import forms

class InterfaceItemsForm(ModelForm):

    class Meta:
        model = InterfaceItems
        fields = ('name', 'url')

class InterfaceParameForm(ModelForm):

    class Meta:
        model = InterfaceParame
        fields = ('parame_key', 'parame_value')

    def clean_parame_key(self):

        key = self.cleaned_data.get('parame_key')
        if key == 'sss':
            raise forms.ValidationError(['sss is error '])
        return key

InterfaceItemInlineFormSet = inlineformset_factory(InterfaceItems, InterfaceParame, form=InterfaceParameForm, extra=1, max_num=10)